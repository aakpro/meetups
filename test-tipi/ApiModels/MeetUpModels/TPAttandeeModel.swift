//
//  TPAttandeeModel.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import ObjectMapper

public class TPAttandeeModel: TPApiBaseModel
{
    var userKey: String?
    var attendedAt: String?
    var city: String?
    var name: String?

    public required init?(map: Map)
    {
        super.init(map: map)
    }
    
    public override func mapping(map: Map)
    {
        super.mapping(map: map)
        userKey                     <- map["user_key"]
        attendedAt                  <- map["attended_at"]
        name                        <- map["name"]
        city                        <- map["city"]
    }
}
