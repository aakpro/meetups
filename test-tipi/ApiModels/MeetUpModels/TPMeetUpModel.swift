//
//  MeetUpModel.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import ObjectMapper

public class TPMeetUpModel: TPApiBaseModel
{
    var docKey: String?
    var userKey: String?
    var title: String?
    var theDescription: String?
    var location: TPLocationModel?
    var address: String?
    var image: String?
    var at: String?
    var hasAttended: Bool?
    var attendees: Array<TPAttandeeModel>?
    var chatKey: String?
    var creator: TPMeetupCreatorModel?
    
    public required init?(map: Map)
    {
        super.init(map: map)
    }
    
    public override func mapping(map: Map)
    {
        super.mapping(map: map)
        docKey                      <- map["doc_key"]
        userKey                     <- map["user_key"]
        title                       <- map["title"]
        theDescription              <- map["description"]
        location                    <- map["location"]
        address                     <- map["address"]
        image                       <- map["image"]
        at                          <- map["at"]
        hasAttended                 <- map["has_attended"]
        attendees                   <- map["attendees"]
        chatKey                     <- map["chat_key"]
        creator                     <- map["creator"]
        
    }
}
