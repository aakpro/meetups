//
//  TPMeetupCreatorModel.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import ObjectMapper

public class TPMeetupCreatorModel: TPApiBaseModel
{
    var docKey: String?
    var name: String?
    var city: String?
    var country: String?

    public required init?(map: Map)
    {
        super.init(map: map)
    }
    
    public override func mapping(map: Map)
    {
        super.mapping(map: map)
        docKey                      <- map["doc_key"]
        name                        <- map["name"]
        city                        <- map["city"]
        country                     <- map["country"]
    }
}
