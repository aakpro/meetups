//
//  TPApiBaseModel.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import ObjectMapper

public class TPApiBaseModel: Mappable
{
    public required init?(map: Map)
    {
    }
    
    public func mapping(map: Map)
    {
    }
}
