//
//  TPError.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//
import ObjectMapper
public enum BTApiErrorTypes
{
    case unknown
}

public class TPError: TPApiBaseModel
{
    public var errorType: BTApiErrorTypes?
    public var statusCode: Int?
    public var serverError: Error?
    public var message: String = "Something went wrong,\n Please try again..."
    
    public required init?(map: Map)
    {
        super.init(map: map)
    }
    public required init?()
    {
        super.init(map: Map(mappingType: .fromJSON, JSON: ["" : ""]))
    }

    public override func mapping(map: Map)
    {
        super.mapping(map: map)
    }
}
