//
//  TPUserModel.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//
import ObjectMapper
public class TPUserModel: TPApiBaseModel
{
    var name: String?
    var country: String?
    var city: String?
    var verificationProgress: Int?
    var interests: Array<String>?
    var docKey: String?
    var auth: String?
    
    public required init?(map: Map)
    {
        super.init(map: map)
    }
    
    public override func mapping(map: Map)
    {
        super.mapping(map: map)
        name                      <- map["name"]
        country                   <- map["country"]
        city                      <- map["city"]
        verificationProgress      <- map["verification_progress"]
        interests                 <- map["interests"]
        docKey                    <- map["doc_key"]
        auth                      <- map["auth"]
    }

}
