//
//  LocationModel.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import ObjectMapper

public class TPLocationModel: TPApiBaseModel
{
    var lat: Double?
    var lon: Double?
    
    public required init?(map: Map)
    {
        super.init(map: map)
    }
    
    public override func mapping(map: Map)
    {
        super.mapping(map: map)
        lat                      <- map["lat"]
        lon                      <- map["lon"]
    }
}
