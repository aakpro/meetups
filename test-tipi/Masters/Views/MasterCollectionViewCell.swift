//
//  MasterCollectionViewCell.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

class MasterCollectionViewCell: UICollectionViewCell
{
    internal static func registerSelf(inCollectionView collectionView:UICollectionView)
    {
        let nibName = String(describing: self)
        collectionView.register(UINib(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: self.reusableIdentifier)
    }
    
    internal static var reusableIdentifier: String
    {
        get{
            return String(describing: self)
        }
    }
    
    //UITableViewAutomaticDimension
    internal class func height() -> CGSize
    {
        return UICollectionViewFlowLayoutAutomaticSize
    }
}
