//
//  MasterTableViewCell.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

internal class MasterTableViewCell: UITableViewCell {
    
    internal static func registerSelf(inTableView tableView:UITableView)
    {
        let nibName = String(describing: self)
        tableView.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: self.reusableIdentifier)
    }
    
    internal static var reusableIdentifier: String
    {
        get{
            return String(describing: self)
        }
    }
    
    //UITableViewAutomaticDimension
    internal class func height() -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
}
