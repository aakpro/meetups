//
//  MasterTableView.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

public class MasterTableView: UITableView
{
    public override func awakeFromNib()
    {
        super.awakeFromNib()
        self.tableFooterView = UIView(frame: .zero)
    }
}
