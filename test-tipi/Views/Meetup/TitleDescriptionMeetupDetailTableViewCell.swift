//
//  TitleDescriptionMeetupDetailTableViewCell.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import SDWebImage
import XCGLogger

internal class TitleDescriptionMeetupDetailTableViewCell: MasterTableViewCell {

    @IBOutlet weak var avatarImageView: MasterImageView!
    @IBOutlet weak var titleLabel: MasterLabel!
    @IBOutlet weak var dateLabel: MasterLabel!
    @IBOutlet weak var descriptionLabel: MasterLabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.avatarImageView?.image = nil
        self.avatarImageView.layer.masksToBounds = true

    }
    override var bounds: CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setCircularImageView()
    }
    
    func setCircularImageView() {
        self.avatarImageView.layer.cornerRadius = CGFloat(roundf(Float(self.avatarImageView.frame.size.width / 2.0)))
    }

    func setContent(meetup: TPMeetUpModel)
    {
        self.avatarImageView.sd_setImage(with: ImageURLUtility.convert(userKey: meetup.userKey!, to: .mediumAvatarURL)) { (image, error, cacheType, url) in
            XCGLogger.default.error(error)
            XCGLogger.default.debug(url)
        }
        
        self.titleLabel.text = meetup.creator?.name
        self.descriptionLabel.text = meetup.theDescription
        self.dateLabel.text = DateTimeUtility.convertMeetupServerDateToMediumHumanReadableDate(meetupStringDate: meetup.at!)
    }
}
