//
//  MapMeeupDetailTableViewCell.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

internal class MapMeeupDetailTableViewCell: MasterTableViewCell, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    func setMap(location: TPLocationModel)
    {
        guard location.lat != nil, location.lon != nil else {
            return
        }
        let clLocation = CLLocation(latitude: location.lat!, longitude: location.lon!)
        self.mapView.setCamera(MKMapCamera(lookingAtCenter: clLocation.coordinate, fromEyeCoordinate: clLocation.coordinate, eyeAltitude: 10000), animated: true)
        mapView.delegate = self
        showCircle(coordinate: clLocation.coordinate, radius: 1000)
        
    }
    
    // Radius is measured in meters
    func showCircle(coordinate: CLLocationCoordinate2D, radius: CLLocationDistance) {
        let circle = MKCircle(center: coordinate, radius: radius)
        mapView.add(circle)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let circleOverlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: circleOverlay)
            circleRenderer.fillColor = UIColor(red:0.15, green:0.75, blue:0.79, alpha:0.2)
            circleRenderer.strokeColor = UIColor(red:0.15, green:0.75, blue:0.79, alpha:1)
            circleRenderer.lineWidth = 1
            return circleRenderer
        }
        fatalError()
    }
}
