//
//  MeetupGuestNumbersCollectionViewCell.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

internal class MeetupGuestNumbersCollectionViewCell: MasterCollectionViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var numberOfAttandeesLabel: UILabel!
    
    @IBOutlet weak var circledBackgroundView: UIView!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.avatarImageView.layer.masksToBounds = true
        self.circledBackgroundView.layer.masksToBounds = true
        self.circledBackgroundView.backgroundColor = ColorUtility.lightGraybackgroundColor
    }

    internal func setAttandee(_ attandee: TPAttandeeModel, attandeesNumber: Int)
    {
        self.numberOfAttandeesLabel.text = "+\(attandeesNumber) traverlers"
        self.setImageWithUrl(ImageURLUtility.convert(userKey: attandee.userKey!, to: .smallAvatarURL).absoluteString)
    }
    
    override var bounds: CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.setCircularImageView()
    }
    
    func setCircularImageView() {
        self.avatarImageView.layer.cornerRadius = CGFloat(roundf(Float(self.avatarImageView.frame.size.width / 2.0)))
        self.circledBackgroundView.layer.cornerRadius = CGFloat(roundf(Float(self.avatarImageView.frame.size.width / 2.0)))

    }
    
    func setImageWithUrl(_ imageUrl:String)
    {
        self.avatarImageView.sd_setImage(with: URL(string:imageUrl)) { (image, error, cacheType, url) in
            
        }
    }
}
