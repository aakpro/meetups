//
//  AttandeesMeetupDetailTableViewCell.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import XCGLogger

internal class AttandeesMeetupDetailTableViewCell: MasterTableViewCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    @IBOutlet weak var userCollectionView: UICollectionView!
    
    var attandees = Array<TPAttandeeModel>()
    override func awakeFromNib()
    {
        super.awakeFromNib()
        MeetupCircledImageAndNameCollectionViewCell.registerSelf(inCollectionView: userCollectionView)
        userCollectionView.delegate = self
        userCollectionView.dataSource = self
    }
    
    func setAttandees(_ attandees: Array<TPAttandeeModel>)
    {
        self.attandees = attandees
        userCollectionView.delegate = self
        userCollectionView.dataSource = self

        self.userCollectionView.reloadData()
    }
    
    @IBAction func pressedSeeAll(_ sender: MasterButton) {
    }
    //MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.attandees.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let attandee = self.attandees[indexPath.row]
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MeetupCircledImageAndNameCollectionViewCell.reusableIdentifier, for: indexPath) as? MeetupCircledImageAndNameCollectionViewCell {
            cell.setAttandee(attandee)
            return cell
        }
        XCGLogger.default.error("error dequing attandee image cell")
        return UICollectionViewCell()
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 76, height: 130)
    }
    
}
