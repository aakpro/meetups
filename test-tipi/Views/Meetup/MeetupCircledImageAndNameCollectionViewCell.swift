//
//  MeetupCircledImageAndNameCollectionViewCell.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import SDWebImage

class MeetupCircledImageAndNameCollectionViewCell: MasterCollectionViewCell {

    @IBOutlet weak var nameLabel: MasterLabel!
    @IBOutlet weak var myImageView: MasterImageView!
    @IBOutlet weak var subNameLabel: MasterLabel!
  
    override var bounds: CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.myImageView.layer.masksToBounds = true
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.setCircularImageView()
    }
    
    func setCircularImageView()
    {
        self.myImageView.layer.cornerRadius = CGFloat(roundf(Float(self.myImageView.frame.size.width / 2.0)))
    }
    
    func setAttandee(_ attandee: TPAttandeeModel)
    {
        self.nameLabel.text = attandee.name ?? "name"
        self.subNameLabel.text = attandee.city ?? "city"
        self.myImageView.sd_setImage(with: ImageURLUtility.convert(userKey: attandee.userKey!, to: .smallAvatarURL)) { (image, error, cacheType, url) in
            
        }
    }
}
