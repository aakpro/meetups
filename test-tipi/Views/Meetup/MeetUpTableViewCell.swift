//
//  MeetUpTableViewCell.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit
import SDWebImage
import XCGLogger

internal class MeetUpTableViewCell: MasterTableViewCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var attandeesCollectionView: MasterCollectionView!
    var attandees = Array<TPAttandeeModel>()
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.mainImageView.image = nil
        self.titleLabel.text = ""
        self.dateLabel.text = ""
        self.attandeesCollectionView.dataSource = self
        self.attandeesCollectionView.delegate = self
        CircleImageCollectionViewCell.registerSelf(inCollectionView: self.attandeesCollectionView)
        MeetupGuestNumbersCollectionViewCell.registerSelf(inCollectionView: self.attandeesCollectionView)
        if let layout = attandeesCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumInteritemSpacing = 10.0
        }
        self.mainImageView.layer.masksToBounds = true
    }
    
    internal func setMeetup(_ meetup: TPMeetUpModel)
    {
        self.titleLabel.text = meetup.title
        self.dateLabel.text = DateTimeUtility.convertMeetupServerDateToMediumHumanReadableDate(meetupStringDate: meetup.at!)
        self.mainImageView.sd_setImage(with: URL(string: meetup.image ?? "")) { (image, error, cacheType, url) in
            self.mainImageView.layer.cornerRadius = 4.0
            XCGLogger.default.error(error)
            XCGLogger.default.verbose(meetup.image)
        }
        if let meetupAttandees = meetup.attendees {
            self.attandees = meetupAttandees
            self.attandeesCollectionView.reloadData()
        }
    }

    //MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.attandees.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let attandee = self.attandees[indexPath.row]
        if indexPath.row == self.attandees.count - 1 {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MeetupGuestNumbersCollectionViewCell.reusableIdentifier, for: indexPath) as? MeetupGuestNumbersCollectionViewCell {
                cell.setAttandee(attandee, attandeesNumber: self.attandees.count)
                return cell
            }
        }
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CircleImageCollectionViewCell.reusableIdentifier, for: indexPath) as? CircleImageCollectionViewCell {
            
            cell.setImageWithUrl(ImageURLUtility.convert(userKey: attandee.userKey!, to: .smallAvatarURL).absoluteString)
            return cell
        }
        XCGLogger.default.error("error dequing attandee image cell")
        return UICollectionViewCell()
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == self.attandees.count - 1 {
            return CGSize(width: 100, height: 22)
        }
        return CGSize(width: 22, height: 22)
    }

}
