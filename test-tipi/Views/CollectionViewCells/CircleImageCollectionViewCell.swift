//
//  CircleImageCollectionViewCell.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import SDWebImage

internal class CircleImageCollectionViewCell: MasterCollectionViewCell {
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    override var bounds: CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.imageView.layer.masksToBounds = true
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setCircularImageView()
    }
    
    func setCircularImageView() {
        self.imageView.layer.cornerRadius = CGFloat(roundf(Float(self.imageView.frame.size.width / 2.0)))
    }

    func setImageWithUrl(_ imageUrl:String)
    {
        self.imageView.sd_setImage(with: URL(string:imageUrl)) { (image, error, cacheType, url) in

        }
    }
}
