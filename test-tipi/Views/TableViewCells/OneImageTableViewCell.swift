//
//  OneImageTableViewCell.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import SDWebImage

internal class OneImageTableViewCell: MasterTableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.MyImageView.image = nil
    }

    @IBOutlet weak var MyImageView: UIImageView!
    
    func setImageWithURL(_ url:URL)
    {
        self.MyImageView.sd_setImage(with: url) { (image, error, cacheType, url) in
            
        }
    }
}
