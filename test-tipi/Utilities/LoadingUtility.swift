//
//  LoginTableViewController.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import PKHUD

public class LoadingUtility: NSObject
{
    public static func showLoading()
    {
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    public static func hideLoading()
    {
        PKHUD.sharedHUD.hide(true)
    }
}
