//
//  SegueIdentifiers.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

internal struct SegueIdentifiers
{
    internal static let fromLoginToPin = "fromLoginToPin"
    internal static let fromMainMeetupToDetailMeetup = "fromMeetupToDetail"
}
