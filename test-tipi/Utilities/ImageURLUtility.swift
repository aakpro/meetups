//
//  ImageURLUtility.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit
enum UserURLTypes {
    case smallAvatarURL
    case generalAvatarURL
    case mediumAvatarURL
}
internal class ImageURLUtility: NSObject
{
    internal static func convert(userKey: String, to urlType: UserURLTypes) -> URL
    {
        if urlType == .smallAvatarURL {
            return URL(string: "http://stg.api.tipi.me/cdn/user/\(userKey)/savatar.jpg")!
        }
        if urlType == .generalAvatarURL {
            return URL(string: "http://stg.api.tipi.me/cdn/all/\(userKey)/savatar.jpg")!
        }
        if urlType == .mediumAvatarURL {
            return URL(string: "http://stg.api.tipi.me/cdn/user/\(userKey)/mavatar.jpg")!
        }
        fatalError()
    }
}
