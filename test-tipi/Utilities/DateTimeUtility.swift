//
//  DateTimeUtility.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import Foundation

internal class DateTimeUtility: NSObject
{
    internal static func convertMeetupServerDateToMediumHumanReadableDate(meetupStringDate dateString:String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateStyle = .medium
        return dateFormatter.string(from: date!)
    }
}
