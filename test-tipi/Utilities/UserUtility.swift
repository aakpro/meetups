//
//  UserUtility.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

internal class UserUtility: NSObject
{
    public static func showLoginView(fromViewController presentingViewController:UIViewController)
    {
            let viewController = UserUtility.makeALoginViewController()
            presentingViewController.present(viewController!, animated: true, completion: nil)
    }
    
    private static func makeALoginViewController() -> UIViewController!
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewControllerSBID") as? UINavigationController
        return loginVC
    }
}
