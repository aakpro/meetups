//
//  MessageUtility.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

public class MessageUtility: NSObject
{
//    public static func ShowErrorAndDoNecessaryActions(error: TPError)
//    {
//        let message = MessageUtility.createMessage(text: error.message)
//        if error.errorType == .needLogin {
//            BTUserLogicHandler().logoutUser()
//        }
//        MDCSnackbarManager.show(message)
//    }
    
    public static func showMessage(_ text: String,from presentingVC: UIViewController)
    {
        let alertController = createRegularAlertController(title: "", message: text, action1Title: "Understood")
        presentingVC.present(alertController, animated: true, completion: nil)

//        let message = MessageUtility.createMessage(text: text)
//        MDCSnackbarManager.show(message)
    }
    
    private static func createRegularAlertController(title: String, message: String, action1Title: String)  -> UIAlertController
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title:action1Title, style: .default) { action in
        }
        alertController.addAction(action)
        return alertController
    }

    
    public static func showMessage(text: String, actionTitle:String, actionHandler:Any)
    {
//        let message = MessageUtility.createMessage(text: text)
//
//        let action = MDCSnackbarMessageAction()
//        let actionHandler = {() in
//        }
//        action.handler = actionHandler
//        action.title = actionTitle
//
//        message.action = action
//        MDCSnackbarManager.show(message)
    }
    
//    private static func createMessage(text:String) -> MDCSnackbarMessage
//    {
//        let message = MDCSnackbarMessage()
//
//        let attributes = [NSAttributedStringKey.font: FontUtility.getFont(forFontType: .regular, andSize: 12), NSAttributedStringKey.foregroundColor: UIColor.white]
//
//        message.attributedText = NSAttributedString(string: text, attributes: attributes)
//        return message
//    }
}
