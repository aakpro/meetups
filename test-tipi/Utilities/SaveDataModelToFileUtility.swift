//
//  SaveDataModelToFileUtility.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import Foundation
import ObjectMapper

public class SaveDataModelToFileUtility
{
    public static func saveMappable<N: Mappable>(mappableObject object: N!, forKey key: String){
        //        print("PersistentStore: Saving \'\(self.fileName())\' (\(object)) to file...")
        if object == nil {
            UserDefaults.standard.set(nil, forKey: key)
            UserDefaults.standard.synchronize()
            return
        }
        let dict = Mapper().toJSON(object!)
        if JSONSerialization.isValidJSONObject(dict) {
            if let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted){
                UserDefaults.standard.set(jsonData, forKey: key)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    public static func loadMappableObject<N: Mappable>(forKey key: String) -> N?{
        if let jsonData = UserDefaults.standard.object(forKey: key) as? Data{
            if let json = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String : Any]{
                return Mapper().map(JSON: json!)
            }
        }
        return nil
    }
}
