//
//  ColorUtility.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/10/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

internal class ColorUtility: NSObject
{
    internal static let lightGraybackgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:0.56)
    internal static let barButtonItemTintColor = UIColor.black
}
