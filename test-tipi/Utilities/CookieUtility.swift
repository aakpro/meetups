//
//  CookieUtility.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/9/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

internal class CookieUtility: NSObject
{
    private let cookieKey = "kCookie"
    internal func setCookie (_ cookie:HTTPCookie)
    {
        UserDefaults.standard.set(cookie.properties, forKey: self.cookieKey)
        UserDefaults.standard.synchronize()
    }
    internal func getCookie () -> HTTPCookie
    {
        let cookie = HTTPCookie(properties: UserDefaults.standard.object(forKey: self.cookieKey) as! [HTTPCookiePropertyKey : Any])
        return cookie ?? HTTPCookie()
    }
    internal func deleteCookie ()
    {
        UserDefaults.standard.set(nil, forKey: self.cookieKey)
        UserDefaults.standard.synchronize()
    }
}
