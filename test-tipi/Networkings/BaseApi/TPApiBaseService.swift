//
//  TPApiBaseService.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import Foundation
import Alamofire

public class TPApiBaseService: NSObject
{
    private let baseUrl = "http://stg.api.tipi.me/v1/"

    public func request(endpoint:String,
                        method:HTTPMethod,
                        headers:Dictionary<String,String>!,
                        body:Dictionary<String,Any>!,
                        response:@escaping (
        _ fullResponse:DataResponse<Any>,
        _ jsonResponse: Dictionary<String, Any?>?,
        _ error:TPError?) -> Swift.Void)
    {
        Alamofire.request("\(baseUrl)\(endpoint)",
            method: method,
            parameters: body,
            encoding: URLEncoding.httpBody,
            headers: headers)
            .responseJSON { (apiResponse) in
                let jsonResponse = apiResponse.value as? Dictionary<String, Any?>
                var error: TPError?
                let statusCode = apiResponse.response?.statusCode
                
                if statusCode != 200 {
                    error = TPError()
                    if statusCode == 400 { error?.message = "Bad request, please try again - code 400" }
                    if statusCode == 404 { error?.message = "Not found - code 404"}
                    if statusCode == 401 { error?.message = "Missing authentication - code 401"}
                }
                
                response(apiResponse, jsonResponse, error)
        }
    }
    
    public func setCookie(_ cookie: HTTPCookie)
    {
        Alamofire.SessionManager.default.session.configuration.httpCookieStorage?.setCookie(cookie)
    }
}
