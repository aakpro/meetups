//
//  TPAuthenticationServices.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//
import Alamofire

public class TPAuthenticationServices: TPApiBaseService
{
    public func requestAuthenticationForEmail(_ email:String, response:@escaping (_ pin: String?, _ error :TPError?) -> Swift.Void)
    {
        let body = ["email":email]
        self.request(endpoint: "users/pin/generate", method: .post, headers: nil, body: body, response: { (fullResponse, jsonResponse, error) in
            guard error == nil else {
                response(nil, error)
                return
            }
            if let json = jsonResponse, let data = json["data"] as? Dictionary<String, Any?>{
                if let pin = data["pin"] as? Int {
                    response("\(pin)", nil)
                    return
                }
            }
            response(nil, TPError())
        })
    }
    
    public func confirmPin(_ pin: String,forEmail email: String, response:@escaping (_ user: TPUserModel?,_ cookie:HTTPCookie?,_ error:TPError?) -> Swift.Void)
    {
        let body = ["email":email, "pin":pin]
        self.request(endpoint: "users/pin/verify", method: .post, headers: nil, body: body, response: { (fullResponse, jsonResponse, error) in
            guard error == nil else {
                response(nil, nil, error)
                return
            }
            if let json = jsonResponse, let data = json["data"] as? Dictionary<String, Any>{
                if let user = TPUserModel(JSON: data ){
                    let cookies = self.fetchTheCookiesFromResponse(fullResponse)
                    response(user, cookies?.first, nil)
                    return
                }
            }
            response(nil, nil, TPError())
        })
    }
    private func fetchTheCookiesFromResponse(_ response:DataResponse<Any>) -> [HTTPCookie]?
    {
        if let headerFields = response.response?.allHeaderFields as? [String: String], let URL = response.request?.url
        {
            let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: URL)
            return cookies
        }
        return nil
    }
    
}
