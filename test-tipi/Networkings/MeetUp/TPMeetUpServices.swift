//
//  TPMeetUpServices.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit
import Foundation

public class TPMeetUpServices: TPApiBaseService
{
    public func getAListOfMeetUps(forLocation location: TPLocationModel?, page: Int, cookie:HTTPCookie, response:@escaping (_ meetUps: Array<TPMeetUpModel>?, _ total:Int?,_ error:TPError?) -> Swift.Void)
    {
        self.setCookie(cookie)
        var query = "page=\(page)"
        if location != nil{
            query = "\(query)&location[lat]=\(location?.lat ?? 0.0)&location[lon]=\(location?.lon ?? 0.0)"
        }
        self.request(endpoint: "user_activities?\(query)", method: .get, headers: nil
        , body: nil) { (fullResponse, jsonResponse, error) in
            guard error == nil else {
                response(nil, nil, error)
                return
            }
            if let json = jsonResponse, let data = json["data"] as? Dictionary<String, Any?>,
                let total = data["total"] as? Int
                , let list = data["list"] as? Array<Dictionary<String, Any>>{
                var meetups = Array<TPMeetUpModel>()
                for object in list {
                    let meetup = TPMeetUpModel(JSON: object)
                    meetups.append(meetup!)
                }
                response(meetups, total, nil)
                return
            }
            response(nil, nil, TPError())
        }
    }
    public func getMeetupDetail(meetupKey: String, cookie:HTTPCookie, response:@escaping (_ meetup: TPMeetUpModel?, _ error:TPError?) -> Swift.Void)
    {
        self.setCookie(cookie)
        self.request(endpoint: "user_activities/\(meetupKey)", method: .get, headers: nil
        , body: nil) { (fullResponse, jsonResponse, error) in
            guard error == nil else {
                response(nil, error)
                return
            }
            if let json = jsonResponse, let data = json["data"] as? Dictionary<String, Any?>{                                    let meetup = TPMeetUpModel(JSON: data)
                response(meetup, nil)
                return
            }
            response(nil, TPError())
        }
    }
}
