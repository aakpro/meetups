//
//  LoginTableViewController.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

internal class LoginTableViewController: MasterTableViewController
{
    @IBOutlet weak var emailTextField: MasterTextField!
    @IBOutlet weak var sendCodeButton: MasterButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        emailTextField.keyboardType = .emailAddress
        emailTextField.becomeFirstResponder()
    }
    
    @IBAction func sendCode(_ sender: MasterButton)
    {
        let email = self.emailTextField.text ?? ""
        guard UserLogicHandlers.isEmailValid(email) == true else {
            MessageUtility.showMessage("Email address is wrong, Please enter a valid email address.", from: self)
            return
        }
        LoadingUtility.showLoading()
        TPAuthenticationServices().requestAuthenticationForEmail(email) { (pin, error) in
            LoadingUtility.hideLoading()
            guard error == nil else {
                MessageUtility.showMessage(error!.message, from:self)
                return
            }
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: SegueIdentifiers.fromLoginToPin, sender: (pin, email))
            }

        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == SegueIdentifiers.fromLoginToPin, let VC =  segue.destination as? ConfirmLoginTableViewController, let data = sender as? (String, String) {
            VC.email = data.1
            VC.pin = data.0
        }
    }
}
