//
//  ConfirmLoginTableViewController.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

internal class ConfirmLoginTableViewController: MasterTableViewController
{
    internal var email = ""
    internal var pin = ""
    @IBOutlet weak var pinTextField: MasterTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pinTextField.keyboardType = .numberPad
        self.pinTextField.becomeFirstResponder()
    }
    
    @IBAction func confirmCode(_ sender: MasterButton) {
        if pinTextField.text == self.pin{
            TPAuthenticationServices().confirmPin(self.pin, forEmail: self.email) { (user, cookie, error) in
                guard error == nil else {
                    MessageUtility.showMessage(error!.message, from:self)
                    return
                }
                UserLogicHandlers.shared.saveUserData(user)
                CookieUtility().setCookie(cookie!)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
