//
//  MasterViewController.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit
import INTULocationManager

internal class MeetUpsTableViewController: MasterTableViewController
{
    var dataSource = [TPMeetUpModel](){
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var location = CLLocation(latitude: 33.868583, longitude: 151.225348)
    
    @IBAction func pressedLogoutButton(_ sender: MasterBarButtonItem)
    {
        UserLogicHandlers.shared.logoutUser()
        UserUtility.showLoginView(fromViewController: self)
    }
    
    @objc private func getAListOfMeetUpsAndUpdateView()
    {
        let location  = TPLocationModel(JSON: ["lat": String(format: "%f", self.location.coordinate.latitude), "lon":String(format: "%f", self.location.coordinate.longitude)])
        TPMeetUpServices().getAListOfMeetUps(forLocation: location, page: 1, cookie: CookieUtility().getCookie()) { (meetUps, total,error) in
            if (self.refreshControl?.isRefreshing)! {
                self.refreshControl?.endRefreshing()
            }
            guard error == nil else {
                MessageUtility.showMessage(error!.message, from:self)
                return
            }
            DispatchQueue.main.async {
                guard meetUps != nil else{
                    return
                }
                self.dataSource = meetUps!
            }
        }
    }
    
    func addPullToRefresh()
    {
        self.refreshControl = UIRefreshControl()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl!)
        }
        refreshControl?.addTarget(self, action: #selector(getAListOfMeetUpsAndUpdateView), for: .valueChanged)
    }
    
    func getLocation ()
    {
        INTULocationManager.sharedInstance().requestLocation(withDesiredAccuracy: .room, timeout: 1.0) { (currentLocation, achievedAccuracy, status) in
            if status == INTULocationStatus.success {
                self.getAListOfMeetUpsAndUpdateView()
            }else {
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.addPullToRefresh()
        self.getLocation()
        MeetUpTableViewCell.registerSelf(inTableView: self.tableView)
        if UserLogicHandlers.shared.isUserLoggedIn{
            self.getAListOfMeetUpsAndUpdateView()
        }
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        if !UserLogicHandlers.shared.isUserLoggedIn{
            UserUtility.showLoginView(fromViewController: self)
        }
    }
    // MARK: - Table View
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: MeetUpTableViewCell.reusableIdentifier, for: indexPath) as? MeetUpTableViewCell{
            cell.setMeetup(self.dataSource[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let meetup = self.dataSource[indexPath.row]
        self.performSegue(withIdentifier: SegueIdentifiers.fromMainMeetupToDetailMeetup, sender: meetup)
    }

    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == SegueIdentifiers.fromMainMeetupToDetailMeetup, let vc = segue.destination as? MeetUpDetailViewController{
            vc.meetup = sender as? TPMeetUpModel
        }
    }
}
