//
//  DetailViewController.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

private enum MeetUpDetailViewControllerSections: Int8 {
    case image = 0
    case nameAndDescription = 1
    case attandees = 2
    case map = 3
}

internal class MeetUpDetailViewController: MasterViewController, UITableViewDelegate, UITableViewDataSource
{

    @IBOutlet weak var tableView: MasterTableView!
    var meetup = TPMeetUpModel(JSON: ["":""])

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        OneImageTableViewCell.registerSelf(inTableView: self.tableView)
        TitleDescriptionMeetupDetailTableViewCell.registerSelf(inTableView: self.tableView)
        AttandeesMeetupDetailTableViewCell.registerSelf(inTableView: self.tableView)
        MapMeeupDetailTableViewCell.registerSelf(inTableView: self.tableView)
        self.fetchMeetupAndUpdateTableView()
        self.tableView.allowsSelection = false
        self.title = "meet-up"
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
    }
    
    func fetchMeetupAndUpdateTableView()
    {
        TPMeetUpServices().getMeetupDetail(meetupKey: (self.meetup?.docKey)!, cookie: CookieUtility().getCookie()) { (meetup, error) in
            guard error == nil else {
                MessageUtility.showMessage(error!.message, from: self)
                return
            }
            DispatchQueue.main.async {
                self.meetup = meetup
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func pressedGoing(_ sender: MasterButton)
    {
    }
    
    //MARK: - Table View Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == Int(MeetUpDetailViewControllerSections.image.rawValue), let cell = tableView.dequeueReusableCell(withIdentifier: OneImageTableViewCell.reusableIdentifier, for: indexPath) as? OneImageTableViewCell{
            cell.setImageWithURL(URL(string:(self.meetup?.image)!)!)
            return cell
        }
        if indexPath.row == Int(MeetUpDetailViewControllerSections.nameAndDescription.rawValue), let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescriptionMeetupDetailTableViewCell.reusableIdentifier, for: indexPath) as? TitleDescriptionMeetupDetailTableViewCell{
            cell.setContent(meetup: self.meetup!)
            return cell
        }
        if indexPath.row == Int(MeetUpDetailViewControllerSections.attandees.rawValue), let cell = tableView.dequeueReusableCell(withIdentifier: AttandeesMeetupDetailTableViewCell.reusableIdentifier, for: indexPath) as? AttandeesMeetupDetailTableViewCell{
            cell.setAttandees((self.meetup?.attendees!)!)
            return cell
        }
        if indexPath.row == Int(MeetUpDetailViewControllerSections.map.rawValue), let cell = tableView.dequeueReusableCell(withIdentifier: MapMeeupDetailTableViewCell.reusableIdentifier, for: indexPath) as? MapMeeupDetailTableViewCell{
            cell.setMap(location: (self.meetup?.location!)!)
            return cell
        }
        fatalError()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == Int(MeetUpDetailViewControllerSections.attandees.rawValue){
            return 180
        }
        return UITableViewAutomaticDimension
    }
}
