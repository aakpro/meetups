//
//  AppDelegate.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit
import XCGLogger

@UIApplicationMain
internal class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        self.setupLogger()
        self.configureColors()
        return true
    }
    
    func configureColors()
    {
        UINavigationBar.appearance().tintColor = ColorUtility.barButtonItemTintColor
    }
    
    private func setupLogger()
    {
        XCGLogger.default.setup(level: .debug, showLogIdentifier: true, showFunctionName:true, showThreadName: true, showLevel: true, showFileNames: true, showLineNumbers: true, showDate: true, writeToFile: nil, fileLevel: nil)
    }
}



