//
//  UserLogicHandlers.swift
//  test-tipi
//
//  Created by Amir Abbas Kashani on 7/7/18.
//  Copyright © 2018 p&c. All rights reserved.
//

import UIKit

internal class UserLogicHandlers: NSObject
{
    static let shared = UserLogicHandlers()
    
    var isUserLoggedIn: Bool{
        return self.getUserIfExists() != nil
    }
    
    public func saveUserData(_ user:TPUserModel!)
    {
        SaveDataModelToFileUtility.saveMappable(mappableObject: user, forKey: ConstantStringsOfModelSavingToFile.userModel)
    }
    public func logoutUser ()
    {
        let user:TPUserModel? = nil
        SaveDataModelToFileUtility.saveMappable(mappableObject: user, forKey: ConstantStringsOfModelSavingToFile.userModel)
        CookieUtility().deleteCookie()
    }
    public func getUserIfExists() -> TPUserModel!
    {
        return SaveDataModelToFileUtility.loadMappableObject(forKey: ConstantStringsOfModelSavingToFile.userModel)
    }

    
    internal static func isEmailValid(_ email: String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: email)
        return result
    }
}
